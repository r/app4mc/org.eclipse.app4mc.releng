/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.release.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile;
import org.eclipse.app4mc.amalthea.validations.sim.App4mcSimProfile;
import org.eclipse.app4mc.amalthea.validations.standard.AmaltheaProfile;
import org.eclipse.app4mc.amalthea.validations.ta.TimingArchitectsProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.util.CachedProfile;
import org.eclipse.app4mc.validation.util.CachedValidator;
import org.eclipse.app4mc.validation.util.ProfileManager;

public class ValidationListGenerator {

	private static List<Class<? extends IProfile>> topLevelProfiles = Arrays.asList(
			AmaltheaProfile.class,
			App4mcSimProfile.class,
			TimingArchitectsProfile.class,
			InchronProfile.class);

	public static void main(String[] args) {

		final File output = new File("output/profiles.textile");

		// ***** Create output directories *****

		try {
			Files.createDirectories(Paths.get(output.getAbsoluteFile().getParent()));
		} catch (IOException e1) {
			System.err.println("Error: Output directory could not be created!");
			return;
		}

		LinkedList<CachedProfile> todo = new LinkedList<>();
		Set<Class<? extends IProfile>> done = new HashSet<>();
		
		// ***** Write content to stream *****

		try (PrintStream ps = new PrintStream(new FileOutputStream(output.getAbsoluteFile(), false)))
		{
			ps.println("h3. Included Validations\n");

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			ps.println("__generated: " + dateFormat.format(new Date()) + "__");

			ProfileManager manager = new ProfileManager();

			for (Class<? extends IProfile> topProfile : topLevelProfiles) {
				CachedProfile cachedProfile = manager.getCachedProfile(topProfile);

				ps.println("\nh4. " + cachedProfile.getName() + "\n");

				todo.addLast(cachedProfile);
				dumpProfiles(todo, done, ps);
			}

		} catch (IOException e) {
			System.err.println("Error: Output file could not be created!");
		}

	}

	private static void dumpProfiles(
			LinkedList<CachedProfile> todo,
			Set<Class<? extends IProfile>> done,
			PrintStream stream) {

		while (!todo.isEmpty()) {
			// remove from list and add to done
			CachedProfile profile = todo.removeFirst();
			if (!done.add(profile.getProfileClass())) continue; // already done

			// Print profile header
			//stream.print("p{padding: 10px; background: #f5f5f5; border: 1px solid #FFF; border-radius: 5px;box-shadow: 1px 2px 4px rgba(0,0,0,.4)}. ");
			stream.print("p{padding: 10px; background: #fafafa; border: 1px solid #888; border-radius: 5px}. ");
			stream.println("@" + profile.getProfileClass().getName() + "@<br/>");
			stream.println("**" + profile.getName() + "**");
			if (!profile.getDescription().isEmpty()) {
				stream.println("__" + profile.getDescription() + "__");
			}

			// Print profile validations
			if (!profile.getCachedValidations().isEmpty()) {
				stream.println("* Validations:");

				// sort validations by severity and name
				List<CachedValidator> sortedValidations = profile.getCachedValidations().values().stream()
						.sorted(Comparator
								.comparing(CachedValidator::getSeverity, Comparator.reverseOrder())
								.thenComparing(CachedValidator::getValidationID, Comparator.naturalOrder()))
						.collect(Collectors.toList());

				for (CachedValidator conf : sortedValidations) {
					stream.println("** " + conf.getValidationID() + " (" + conf.getSeverity() + " - "
							+ conf.getTargetClass().getSimpleName() + ")");

					// sort checks alphabetically
					List<String> sortedChecks = Arrays.asList(conf.getValidationChecks()).stream().sorted().collect(Collectors.toList());

					for (String check : sortedChecks) {
						stream.println("*** " + check);
					}
				}
			}

			// Print profile sub profiles
			if (!profile.getCachedProfiles().isEmpty()) {
				stream.println("* Profiles:");

				// sort sub profiles by class name
				List<CachedProfile> sortedSubProfiles = profile.getCachedProfiles().values().stream()
						.sorted(Comparator.comparing(
								CachedProfile::getProfileClass, (a, b) -> a.getName().compareTo(b.getName())))
						.collect(Collectors.toList());

				// list sub profiles (and add to list for further processing)
				for (CachedProfile subProfile : sortedSubProfiles) {
					stream.println("** @" + subProfile.getProfileClass().getName() + "@");
					todo.addLast(subProfile);
				}
			}
		}
	}

}
