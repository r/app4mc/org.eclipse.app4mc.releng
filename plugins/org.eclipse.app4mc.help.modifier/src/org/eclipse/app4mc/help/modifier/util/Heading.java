/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier.util;

public class Heading {
	public Integer level;
	public String number;
	public String text;

	public Heading() {
		super();
	}

	public Heading(Integer level, String number, String text) {
		super();
		this.level = level;
		this.number = number;
		this.text = text;
	}

	@Override
	public String toString() {
		return "level " + level + " >" + number + "< >" + text + "<";
	}
}
