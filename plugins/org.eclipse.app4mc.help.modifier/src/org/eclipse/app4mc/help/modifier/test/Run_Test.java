/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier.test;

import java.io.File;
import java.net.URL;

import org.eclipse.app4mc.help.modifier.APP4MC_Help_Step_1_UpdateTOC;
import org.eclipse.app4mc.help.modifier.APP4MC_Help_Step_2_UpdateContent;

public class Run_Test {

	public static void main(final String[] args) throws Exception {

		URL classLoc = Run_Test.class.getProtectionDomain().getCodeSource().getLocation();
		String path = new File(classLoc.getPath()).getParent();
		String sep = File.separator;
		String inputFilePath = path + sep + "test-files" + sep + "print22dc.html";

		// STEP 1
		final File fileStep1 = APP4MC_Help_Step_1_UpdateTOC.execute(inputFilePath);

		// STEP 2
		APP4MC_Help_Step_2_UpdateContent.execute(fileStep1.getAbsolutePath());
	}
}
