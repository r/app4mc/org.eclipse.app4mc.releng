/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier;

public class APP4MC_Help_Step_3_CopyFiles {

	public static void main(final String[] args) {

		//===============================================
		// 					TODO
		//===============================================
		//	
		//  move javadoc files to javadoc folder
		//	update links
		//		
		//	move zip files to zip folder
		//	update links
		//	
		//	move help.css to css folder
		//	delete other help.css files
		//	
		//	use template
		//

	}

}
