/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier;

import java.io.File;

public class Run_automated_steps {

	public static void main(final String[] args) throws Exception {

		String inputFilePath = ".../help-raw/APP4MC Help 2.1.0/web/printc223.html";

		// STEP 1
		final File fileStep1 = APP4MC_Help_Step_1_UpdateTOC.execute(inputFilePath);
		
		// STEP 2
		APP4MC_Help_Step_2_UpdateContent.execute(fileStep1.getAbsolutePath());
	}
}
